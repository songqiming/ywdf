# ywdf

#### 介绍
一款公共图片上传和读取的小工具

#### 软件架构
基于mybatis-plus和spring-boot项目


#### 安装教程

用项目中的sql创建数据库，修改数据库连接
讲prop-web.yml文件中的imagePathPrefix修改为需要的路径，启动项目就OK了

#### 使用说明

提供两个接口

**/pub/uploadImage(上传图片接口)**
|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|file |是  |MultipartFile |图片文件   |
|type |否  |string | 自定义图片存放归类，比如机构图片为org    |
|isResize     |否  |string | 是否剪裁缩略图（true-是），默认不剪裁，剪裁后才能在获取图片时才能获取缩略图    |
|width     |否  |int | 缩略图宽（如果isResize为true则不可为空）    |
|height     |否  |int | 缩略高度（如果isResize为true则不可为空）    |
 **返回示例**

``` 
{
  "retCode": "0000",
  "retMsg": "操作成功",
  "data": {
    "imageId": 10
  }
}
```
 **返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|imageId |int   |图片id  |


**/pub/getImage(读取图片接口)**

直接在浏览器输入：
http://XXXX/pub/getImage?id=XX
剪裁图片：
http://XXXX/pub/getImage?id=XX&isResize=true
如果想放入<img>标签，把链接写到src属性中即可

#### 图片存储说明
图片会存在prop-web.yml中imagePathPrefix路径下，按照上传图片接口的type参数以月为单位存储，缩略图存在tumb目录下
