package com.ywdf.service.pub;

import com.baomidou.mybatisplus.service.IService;
import com.ywdf.model.Image;

import java.io.File;

public interface ImageService extends IService<Image> {

    //保存一般图片，若需自定义缩略图大小，需指定minwidth，minheight属性
    public Image saveImage(File destFile, Image image, boolean isResize) ;

    /**
     * 获取目标对象
     * @param originFileName - 文件名
     * @param type - 文件类型(区分不同地方上传过来的文件)
     * @return
     */
    public Image getImage(String originFileName, String type);

    //获取目标文件
    public File getDestFile(String OriginFilename);

}
