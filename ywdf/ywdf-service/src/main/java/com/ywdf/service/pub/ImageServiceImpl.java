package com.ywdf.service.pub;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ywdf.mapper.ImageMapper;
import com.ywdf.model.Image;
import com.ywdf.prop.WebProperties;
import com.ywdf.utils.DigestUtil;
import com.ywdf.utils.UploadUtil;
import com.ywdf.utils.img.AverageImageScale;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;

/**
 * @author songqiming
 */
@Service
@Slf4j
public class ImageServiceImpl extends ServiceImpl<ImageMapper, Image> implements ImageService {

    @Autowired
    private WebProperties properties;

    @Override
    public Image getImage(String originFileName, String type) {
        Assert.notNull(originFileName);
        Assert.notNull(type);
        //获取后缀
        String ext = FilenameUtils.getExtension(originFileName).toLowerCase(Locale.ENGLISH);
        //生成唯一的路径及文件名
        String normalPath = UploadUtil.generateByExt(ext);
        Image image = new Image();
        image.setOriginFileName(originFileName);
        image.setExt(ext);
        image.setNormalPath("/" + type + normalPath);
        return image;
    }

    @Override
    public File getDestFile(String normalPath) {
        File file = UploadUtil.getUniqueFile(getRealFile(normalPath));
        UploadUtil.checkDirAndCreate(file.getParentFile());
        return file;

    }

    /**
     * 保存一般图片
     *
     * @param destFile
     * @param image    若需自定义缩略图大小，需指定minwidth，minheight属性
     * @param isResize
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Image saveImage(File destFile, Image image, boolean isResize) {
        Assert.notNull(destFile);
        Assert.notNull(image);
        init(destFile, image);
        if (isResize) {
            resizeimage(destFile, image, "/thumb");
        }
        Date date = new Date();
        image.setCreateTime(date);
        image.setUpdateTime(date);
        baseMapper.insert(image);
        return image;
    }

    private void init(File destFile, Image image) {
        image.setFileName(destFile.getName());
        image.setNormalFileSize(destFile.length());
        //文件摘要
        image.setSubject(DigestUtil.digestHex(destFile));
        image.setCreateTime(new Timestamp(System.currentTimeMillis()));
        try {
            //图片流获取宽高
            BufferedImage bufferedImg = ImageIO.read(new FileInputStream(destFile));
            image.setNormalWidth(bufferedImg.getWidth());
            image.setNormalHeight(bufferedImg.getHeight());
        } catch (Exception e) {
            image.setNormalWidth(200);
            image.setNormalHeight(200);
            log.error(String.format("获取图片宽高异常，文件名为%s", image.getFileName()), e);
        }
    }

    /**
     * 保存缩略图及image
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public void resizeimage(File destFile, Image image, String pre) {
        Integer minWidth = image.getMinWidth();
        Integer minHeight = image.getMinHeight();
        if (minWidth == null) {
            minWidth = Integer.parseInt(properties.getImageResizeWidth());
            image.setMinWidth(minWidth);
        }
        if (minHeight == null) {
            minHeight = Integer.parseInt(properties.getImageResizeHeight());
            image.setMinHeight(minHeight);
        }
        String thumbName = destFile.getName().replace(".", String.format("_%s_%s.", minWidth, minHeight));
        String minPath = pre + UploadUtil.generateMonthName(thumbName);
        File thumbFile = UploadUtil.createUniqueFile(getRealFile(minPath));
        try {
            AverageImageScale.resizeFix(destFile, thumbFile.getCanonicalFile(), minWidth, minHeight);
            image.setMinFileSize(thumbFile.length());
            image.setMinPath(minPath);
        } catch (Exception e) {
            log.error(String.format("保存缩略图IO异常，文件名为%s", image.getFileName()), e);
            if (destFile != null) {
                destFile.delete();
            }
        }
    }

    private File getRealFile(String normalPath) {
        String imagePathPrefix = properties.getImagePathPrefix();
        String imageUploadPath = properties.getImageUploadPath();
        String realpath = imagePathPrefix + imageUploadPath + normalPath;
        return new File(realpath);
    }

}
