package com.ywdf.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Data
@Component
@ConfigurationProperties(locations = "classpath:prop-web.yml")
public class WebProperties {


    /**
     * 图片上传相对路径
     */
    private String imageUploadPath;
    /**
     * 图片上传根路径
     */
    private String imagePathPrefix;
    /**
     * 缩略图宽
     */
    private String imageResizeWidth;
    /**
     * 缩略图高
     */
    private String imageResizeHeight;



    
}
