package com.ywdf.utils;

import com.ywdf.enums.ApiResultStatus;
import com.ywdf.enums.ExceptionEnum;
import lombok.Data;

/**
 * 返回值封装
 *
 * @author songqiming
 */
@Data
public class ApiResultMessage<T> {

    /**
     * api返回值
     *
     * @param status 返回状态
     * @param data   返回数据
     * @return api返回值
     */
    public static <T> ApiResultMessage<T> createResult(ApiResultStatus status, T data) {
        return new ApiResultMessage<>(status.getErrCode(), status.getErrMsg(), data);
    }

    /**
     * api返回值
     *
     * @param exceptionEnum 返回状态
     * @param data          返回数据
     * @return api返回值
     */
    public static <T> ApiResultMessage<T> createResult(ExceptionEnum exceptionEnum, T data) {
        return new ApiResultMessage<>(exceptionEnum.getErrCode(), exceptionEnum.getErrMsg(), data);
    }


    /**
     * api返回默认成功
     *
     * @param data 返回数据
     * @return api返回值
     */
    public static <T> ApiResultMessage<T> createResult(T data) {
        return createResult(ExceptionEnum.DEFULT_CODE_0000, data);
    }


    public static <T> ApiResultMessage<T> createResult(String retCode, String retMsg, T data) {
        return new ApiResultMessage<>(retCode, retMsg, data);
    }

    private String retCode;
    private String retMsg;
    private T data;

    public ApiResultMessage() {
    }

    public ApiResultMessage(String retCode, String retMsg) {
        this.retCode = retCode;
        this.retMsg = retMsg;
    }

    public ApiResultMessage(String retCode, String retMsg, T data) {
        this.retCode = retCode;
        this.retMsg = retMsg;
        this.data = data;
    }


}
