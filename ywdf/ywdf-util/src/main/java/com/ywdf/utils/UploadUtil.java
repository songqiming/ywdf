package com.ywdf.utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;


public class UploadUtil {
	/**
	 * 日期格式化对象
	 */
	public static final DateFormat MONTH_FORMAT = new SimpleDateFormat("/yyyyMM/ddHHmmssSSS");
	
	public static final DateFormat YEAR_MONTH_FORMAT = new SimpleDateFormat("/yyyyMM/");
	
	public static final DateFormat SHORT_FORMAT = new SimpleDateFormat("ddHHmmss");
	
	protected static final Pattern ILLEGAL_CURRENT_FOLDER_PATTERN = Pattern
			.compile("^[^/]|[^/]$|/\\.{1,2}|\\\\|\\||:|\\?|\\*|\"|<|>|\\p{Cntrl}");
	
	/**
	 * 当前年月yyyyMM
	 */
	public static String generateMonthName(String fileName) {
		return YEAR_MONTH_FORMAT.format(new Date()) + fileName;
	}

	/**
	 * 根据缀随机生成唯一的路径及文件名 /yyyyMM/ddHHmmss
	 */
	public static String generateByExt(String ext) {
		return MONTH_FORMAT.format(new Date())
				+ RandomStringUtils.random(4, Num62.N36_CHARS) + "." + ext;
	}
	
	/**
	 * 根据文件名生成唯一的路径
	 */
	public static String generateByFileName(String fileName) {
		return MONTH_FORMAT.format(new Date()) + fileName;
	}
	
	/**
	 * 创建唯一文件
	 */
	public static File createUniqueFile(File file){
		File uniqueFile = getUniqueFile(file);
		checkDirAndCreate(uniqueFile.getParentFile());
		return uniqueFile;
	}

	/**
	 * 去除文件名中的特殊字符
	 */
	public static String sanitizeFileName(final String filename) {
		if (StringUtils.isBlank(filename))
			return filename;
		String name = forceSingleExtension(filename);
		return name.replaceAll("\\\\|/|\\||:|\\?|\\*|\"|<|>|\\p{Cntrl}", "_");
	}

	/**
	 * 去除文件夹中的特殊字符
	 */
	public static String sanitizeFolderName(final String folderName) {

		if (StringUtils.isBlank(folderName))
			return folderName;
		return folderName.replaceAll(
				"\\.|\\\\|/|\\||:|\\?|\\*|\"|<|>|\\p{Cntrl}", "_");
	}

	/**
	 * 是否符合要求的路径
	 */
	public static boolean isValidPath(final String path) {
		if (StringUtils.isBlank(path))
			return false;

		if (ILLEGAL_CURRENT_FOLDER_PATTERN.matcher(path).find())
			return false;
		return true;
	}

	/**
	 * 替换特殊字符为下划线
	 */
	public static String forceSingleExtension(final String filename) {
		return filename.replaceAll("\\.(?![^.]+$)", "_");
	}

	/**
	 * 检查文件名是否包含多个点
	 */
	public static boolean isSingleExtension(final String filename) {
		return filename.matches("[^\\.]+\\.[^\\.]+");
	}

	/**
	 * 文件不存在则创建
	 */
	public static void checkDirAndCreate(File dir) {
		if (!dir.exists())
			dir.mkdirs();
	}

	/**
	 * 获取同一级下最后一个相同文件名的文件
	 */
	public static File getUniqueFile(final File file) {
		if (!file.exists())
			return file;

		File tmpFile = new File(file.getAbsolutePath());//获取绝对路径
		File parentDir = tmpFile.getParentFile();
		int count = 1;
		String ext = FilenameUtils.getExtension(tmpFile.getName());
		String baseName = FilenameUtils.getBaseName(tmpFile.getName());
		do {
			tmpFile = new File(parentDir, baseName + "(" + count++ + ")."
					+ ext);
		} while (tmpFile.exists());
		return tmpFile;
	}

}
