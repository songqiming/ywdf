package com.ywdf.enums;

/**
 * @discription:信息发送模板编号及描述
 * @author:yhGuo
 * @date 2019/5/28 15:02
 */
public enum MsgSendTplEnum {



    INFO_SEND_PRI("01","平台个人推送模板"),
    INFO_SEND_MAIL("02","邮件推送模板"),
    INFO_SEND_SMS("03","短信推送模板"),
    INFO_SEND_PUB("04","平台公共信息推送模板");
    /**
     * 模板编号
     */
    private String tplCode;
    /**
     * 模板描述
     */
    private String tplCodeMsg;

    MsgSendTplEnum(String tplCode,String tplCodeMsg){
        this.tplCode = tplCode;
        this.tplCodeMsg = tplCodeMsg;
    }


    public String getTplCode() {
        return tplCode;
    }

    public String getTplCodeMsg() {return tplCodeMsg;}
}
