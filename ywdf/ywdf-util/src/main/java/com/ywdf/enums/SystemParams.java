package com.ywdf.enums;

public class SystemParams {
    /**
     * 正常
     */
    public static int DEL_STA_NORMAL = 1;
    /**
     * 删除
     */
    public static int DEL_STA_FALSE = 0;

    public static String TOKEN = "token";

    public static String USERNAME = "userName";

    /**
     * 机构信息机构状态 0：初稿 1：修改  2：审核  3：合作中  4：停止合作 默认为3
     */
    public static int ORG_STA_FIRST_DRAFT = 0;

    public static int ORG_STA_MERGE = 1;

    public static int ORG_STA_VERIFY = 2;

    public static int ORG_STA_COOPERATION = 3;

    public static int ORG_STA_UNCOOPERATION = 4;
    /**
     * 机构类型 1-渠道 0-内容
     */
    public static int ORG_TYPE_CHANNEL = 1;
    public static int ORG_TYPE_CONTENT = 0;

    /**
     * 内容状态
     */
    public static int CONTENT_STA_CHUANGJIAN = 0;
    public static int CONTENT_STA_PIPEI = 1;
    public static int CONTENT_STA_XIAFA = 2;
    public static int CONTENT_STA_END = 3;

    /**
     * 内容发送管理状态
     */
    public static int TERMINAL_ORG_CONT_YIPIPEI = 0;
    public static int TERMINAL_ORG_CONT_HEZUOZHONG = 1;
    public static int TERMINAL_ORG_CONT_YIJIESHU = 2;

    /**
     * 消息类型   0：个人消息  1：公共消息
     */
    public static int INFO_TYPE_MESSAGE = 0;
    public static int INFO_TYPE_ANNOUNCEMENT = 1;
    /**
     * 信息是否已读 0：未读  1：已读
     */
    public static int INFO_IS_UNREAD = 0;
    public static int INFO_IS_READ = 1;

    /**
     * 网站地址合规校验
     */
    public static String WEBSITE_REGULAR_URL = "http://101.230.236.191:8080/ocr/websiteRegular";

    /**
     * 分隔符
     */
    public static String separator = ",";

    // ===== [start]网站地址合规性校验相关配置项 ======

    /**
     * 网站地址合规校验功能开关
     */
    public static String WEBSITE_REGULAR_SWITCH_KEY = "WEBSITE_REGULAR_SWITCH_KEY";
    /**
     * 安全评分提醒功能开关
     */
    public static String SAFETY_RISK_REMIND_SWITCH_KEY = "SAFETY_RISK_REMIND_SWITCH_KEY";
    /**
     * 安全评分提醒分数设置
     */
    public static String SAFETY_RISK_REMIND_SCORE_KEY = "SAFETY_RISK_REMIND_SCORE_KEY";
    /**
     * 安全评分提醒消息人配置（多个英文以逗号分隔）
     */
    public static String SAFETY_RISK_REMIND_EMAILS_KEY = "SAFETY_RISK_REMIND_EMAILS_KEY";
    /**
     * 网站地址合规校验白名单功能开关
     */
    public static String WEBSITE_REGULAR_WHITE_LIST_SWITCH_KEY = "WEBSITE_REGULAR_WHITE_LIST_SWITCH_KEY";
    /**
     * 网站地址合规校验白名单模糊匹配项（多个以英文逗号分隔）
     */
    public static String WEBSITE_REGULAR_WHITE_LIST_FUZZY_MATCH_KEY = "WEBSITE_REGULAR_WHITE_LIST_FUZZY_MATCH_KEY";
    /**
     * 安全白名单全匹配项（多个以英文逗号分隔）
     */
    public static String WEBSITE_REGULAR_WHITE_LIST_EXACT_MATCH_KEY = "WEBSITE_REGULAR_WHITE_LIST_EXACT_MATCH_KEY";
    /**
     * 高风险内容禁用功能开关
     */
    public static String HIGH_RISK_CONTENT_DISABLE_SWITCH_KEY = "HIGH_RISK_CONTENT_DISABLE_SWITCH_KEY";
    /**
     * 删除内容链接合规性校验日志的超限天数(大于此天数的日志记录将被删除)
     */
    public static String WEBSITE_REGULAR_DELETE_OUT_DAY_KEY = "WEBSITE_REGULAR_DELETE_OUT_DAY_KEY";

    // ===== [end]网站地址合规性校验相关配置项 ======

    // ===== [start]支付涉及相关常量 ======
    /**
     * 支付接口 订单来源 1-申码录入  2-接口录入
     */
    public static Integer ORDER_SOURCES_APPLY = 1;

    public static Integer ORDER_SOURCES_INTER = 2;

    /**
     * 订单状态 0-申码 1-支付成功 2-支付失败 3-已经勾兑
     */
    public static Integer TRANSACTION_INFO_STATUS_APPLICATION_CODE = 0;
    public static Integer TRANSACTION_INFO_STATUS_PAY_SUCCESS = 1;
    public static Integer TRANSACTION_INFO_STATUS_PAY_FAIL = 2;
    public static Integer TRANSACTION_INFO_STATUS_CHECKED = 3;

    /**
     * 勾兑结果，0：勾兑失败，1：勾兑成功
     */
    public static Integer PAY_CHECK_RESULT_SUCCESS = 1;
    public static Integer PAY_CHECK_RESULT_FAIL = 0;

    /**
     * 支付结果 -1：申码（未支付） 0：支付成功 1：支付失败
     */
    public static Integer PAY_RESULT_APPLICATION_CODE = -1;
    public static Integer PAY_RESULT_SUCCESS = 0;
    public static Integer PAY_RESULT_FAIL = 1;

    // ===== [end]支付涉及相关常量 ======
}
