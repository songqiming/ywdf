package com.ywdf.enums;

/**
 * 网站地址合规校验枚举
 */
public enum WebsiteRegularEnum {
    /**
     * 返回结果状态
     */
    REQUEST_CODE_SUCCESS("00", "成功"),
    REQUEST_CODE_FAIL("98", "失败"),

    /**
     * 风险级别
     */
    RISK_LEVEL_PASS("PASS", "正常内容，建议直接放行"),
    RISK_LEVEL_REVIEW("REVIEW", "可疑内容，建议⼈工审核"),
    RISK_LEVEL_REJECT("REJECT", "违规内容，建议直接拦截"),

    /**
     * 合规性校验结果
     */
    REGULAR_RESULT_FAIL("-1", "校验失败"),
    REGULAR_RESULT_PASS("0", "正常内容，建议直接放行"),
    REGULAR_RESULT_REVIEW("1", "可疑内容，建议⼈工审核"),
    REGULAR_RESULT_REJECT("2", "违规内容，建议直接拦截"),
    ;

    private String value;
    private String msg;

    WebsiteRegularEnum(String value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public String getValue() {
        return value;
    }

    public String getMsg() {
        return msg;
    }
}
