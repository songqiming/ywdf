package com.ywdf.enums;

/**
 * 所有返回值枚举类都实现此接口
 */
public interface ApiResultStatus {

//    int getStatus();
    String getErrCode();
    String getErrMsg();
}
