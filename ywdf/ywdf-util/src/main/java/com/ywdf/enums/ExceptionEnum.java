package com.ywdf.enums;

/**
 * Class Name : ExceptionEnum<br>
 *
 * @author songqiming
 * @version $Revision$
 * @see
 */
public enum ExceptionEnum implements ApiResultStatus {

    //默认异常
    DEFULT_CODE_0000("0000", "操作成功"),


    ///////////////////系统级异常///////////////////
    SYS_ERROR_CODE_9999("9999", "系统异常"),
    SYS_ERROR_CODE_9998("9998", "请求参数错误"),
    SYS_ERROR_CODE_9996("9996", "请定义图片宽高"),
    SYS_ERROR_CODE_9995("9995", "请定义图片类型"),
    SYS_ERROR_CODE_9993("9993", "仅支持JPG、PNG格式的图片文件"),

    ;
    /**
     * 异常code
     */
    private String errCode;

    /**
     * 异常描述信息
     */
    private String errMsg;

    ExceptionEnum(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    @Override
    public String getErrCode() {
        return errCode;
    }

    @Override
    public String getErrMsg() {
        return errMsg;
    }

}
