package com.ywdf.exception;

import com.ywdf.enums.ExceptionEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Auther:songqiming
 * @Description:拦截全局系统异常
 * @Date: 11:55 2018/5/31
 */
@ControllerAdvice
class GlobalExceptionHandler {

    public static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    public static final String DEFAULT_ERROR_VIEW = "error";

    /**
     * @Auther:chenzhen
     * @Description:系统全局异常
     * @Date: 13:04 2018/5/31
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultBean defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        logger.error("system error : {}|{}", req.getServletPath(), e);
        return new ResultBean(ExceptionEnum.SYS_ERROR_CODE_9999.getErrCode(), ExceptionEnum.SYS_ERROR_CODE_9999.getErrMsg());
    }

    /**
     * @Auther:songqm
     * @Description:非空参数校验异常
     * @Date: 2019-4-29 15:01:23
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public ResultBean bindingErrorHandler(HttpServletRequest req, MethodArgumentNotValidException e) {
        logger.error("参数校验 error : {}|{}", req.getServletPath(), e);
        List<ObjectError> list = e.getBindingResult().getAllErrors();
        StringBuilder msg = new StringBuilder();
        for (ObjectError objectError : list) {
            msg.append(objectError.getCodes()[0]).append("|");
        }
        msg.insert(0, ExceptionEnum.SYS_ERROR_CODE_9998.getErrMsg());
        return new ResultBean(ExceptionEnum.SYS_ERROR_CODE_9998.getErrCode(), msg.toString());
    }


    /**
     * @Auther:songqiming
     * @Description:业务异常
     * @Date: 13:04 2018/5/31
     */
    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ResultBean parameterException(HttpServletRequest req, BusinessException e) {
        logger.error("业务异常 login error : {}|{}", req.getServletPath(), e);
        return new ResultBean(e.getErrCode(), e.getMessage());
    }

}