package com.ywdf.exception;


import com.ywdf.enums.ExceptionEnum;

/**
 * 业务异常类
 */
public class BusinessException extends BaseException {

    private static final long serialVersionUID = -4987001305340759909L;


    private BusinessException() {
        super();
    }

    private BusinessException(String errCode) {
        super(errCode);
    }

    private BusinessException(String errCode, String... params) {
        super(errCode, params);
    }

    private BusinessException(String errCode, String message) {
        super(errCode, message);
    }

    /**
     * <pre>
     * 抛出业务逻辑异常信息
     * </pre>
     *
     * @param errCode 异常码
     * @param params  异常信息
     */
    public static void throwMessage(String errCode, String... params) {
        throw new BusinessException(ExceptionEnum.SYS_ERROR_CODE_9998.getErrCode(), errCode);
    }

    /**
     * <pre>
     * 抛出业务逻辑异常信息
     * </pre>
     *
     * @param errCode 异常码
     * @param message 异常信息
     */
    public static void throwMessageWithCode(String errCode, String message) {
        throw new BusinessException(errCode, message);
    }

    /**
     * <pre>
     * 抛出业务逻辑异常信息
     * </pre>
     */
    public static void throwMessageWithCode(ExceptionEnum exceptionEnum) {
        String errCode = String.valueOf(exceptionEnum.getErrCode());
        String message = exceptionEnum.getErrMsg();
        throw new BusinessException(errCode, message);
    }

}
