package com.ywdf.exception;

import com.ywdf.enums.ExceptionEnum;
import lombok.Data;

import java.io.Serializable;

@Data
public class ResultBean<T> implements Serializable {
    private static final long serialVersionUID = 1L;


    private String retMsg = ExceptionEnum.DEFULT_CODE_0000.getErrCode();
    private String retCode = ExceptionEnum.DEFULT_CODE_0000.getErrMsg();
    private T data;

    public ResultBean() {
        super();
    }

    public ResultBean(T data){
        super();
        this.data = data;
    }
    public ResultBean(Throwable e){
        super();
        this.retMsg = e.toString();
        this.retCode = ExceptionEnum.DEFULT_CODE_0000.getErrCode();
    }
    public ResultBean(String code, String msg){
        super();
        this.retMsg = msg;
        this.retCode = code;
    }
}
