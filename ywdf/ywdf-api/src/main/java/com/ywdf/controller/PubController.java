package com.ywdf.controller;

import com.ywdf.enums.ExceptionEnum;
import com.ywdf.exception.BusinessException;
import com.ywdf.model.Image;
import com.ywdf.prop.WebProperties;
import com.ywdf.service.pub.ImageService;
import com.ywdf.utils.ApiResultMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 公共类接口
 */
@Controller
@RequestMapping("/pub")
public class PubController {
    @Autowired
    private ImageService imageService;
    @Autowired
    private WebProperties prop;

    /**
     * 获取图片接口
     *
     * @param request
     * @param id       图片id
     * @param isResize 是否获取缩略图（只有上传时选择了剪裁图片才有缩略图）
     * @param response
     */
    @RequestMapping(value = "/getImage")
    @ResponseBody
    public void uploadImage(HttpServletRequest request, Long id, String isResize, HttpServletResponse response) {
        Image image = imageService.selectById(id);
        FileInputStream fis = null;
        String path = prop.getImagePathPrefix() + prop.getImageUploadPath();
        response.setContentType("image/gif");
        try {
            OutputStream out = response.getOutputStream();
            File file = null;
            if (StringUtils.isBlank(isResize)) {
                file = new File(path + image.getNormalPath());
            } else {//获取缩略图
                if (!StringUtils.isBlank(image.getMinPath())) {
                    file = new File(path + image.getMinPath());
                } else {
                    file = new File(path + image.getNormalPath());
                }
            }
            fis = new FileInputStream(file);
            byte[] b = new byte[10];
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            int i = 0;
            while ((i = bis.read(b, 0, b.length)) != -1) {
                out.write(b, 0, i);
                out.flush();
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 上传图片接口
     *
     * @param request
     * @param picFile  图片文件
     * @param type     自定义图片存放归类，比如机构图片为org
     * @param isResize 是否剪裁缩略图（true-是），默认不剪裁，剪裁后才能在获取图片时才能获取缩略图
     * @param width    缩略图宽（如果isResize为true则不可为空）
     * @param height   缩略高度（如果isResize为true则不可为空）
     * @return
     */
    @RequestMapping(value = "/uploadImage")
    @ResponseBody
    public ApiResultMessage<Map<String, Object>> uploadImage(HttpServletRequest request, @RequestParam("file") MultipartFile picFile, String type, String isResize, Integer width, Integer height) {
        boolean flag = !StringUtils.isBlank(isResize);
        Map<String, Object> response = new HashMap<>();
        Map<String, Object> returnMap = new HashMap<>();
        //如果需要缩略图，则需定义长宽高
        if (flag) {
            if (width == null || height == null) {
                BusinessException.throwMessageWithCode(ExceptionEnum.SYS_ERROR_CODE_9996);
            }
        }
        if (StringUtils.isBlank(type)) {
            BusinessException.throwMessageWithCode(ExceptionEnum.SYS_ERROR_CODE_9995);
        }
        try {
            Image image = imageService.getImage(picFile.getOriginalFilename(), type);
            if(!"jpg".equals(image.getExt())&&!"png".equals(image.getExt())){
                return ApiResultMessage.createResult(ExceptionEnum.SYS_ERROR_CODE_9993, null);
            }
            File file = imageService.getDestFile(image.getNormalPath());
            picFile.transferTo(file);
            returnMap.put("imageId", imageService.saveImage(file, image, flag).getId());
        } catch (Exception e) {
            e.printStackTrace();
            BusinessException.throwMessageWithCode(ExceptionEnum.SYS_ERROR_CODE_9999);
        }

        return ApiResultMessage.createResult(returnMap);
    }

}
