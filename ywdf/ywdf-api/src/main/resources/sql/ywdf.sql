

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `del_sta` tinyint(1) DEFAULT '1' COMMENT '1-正常 0-删除',
  `update_time` datetime DEFAULT NULL,
  `create_by` bigint(11) DEFAULT NULL COMMENT '创建人',
  `title` varchar(200) DEFAULT NULL COMMENT '主题',
  `subject` varchar(32) DEFAULT NULL COMMENT '摘要',
  `ext` varchar(30) DEFAULT NULL COMMENT '扩展名',
  `file_name` varchar(200) DEFAULT NULL COMMENT '文件名称',
  `origin_file_name` varchar(200) DEFAULT NULL COMMENT '原文件名称',
  `normal_file_size` int(12) DEFAULT NULL COMMENT '文件字节数',
  `normal_height` int(12) DEFAULT NULL COMMENT '高度-正常',
  `normal_width` int(12) DEFAULT NULL COMMENT '宽度-正常',
  `normal_path` varchar(200) DEFAULT NULL COMMENT '正常图片路径',
  `max_file_size` int(12) DEFAULT NULL COMMENT '文件字节数-最大',
  `max_height` int(12) DEFAULT NULL COMMENT '高度-最大',
  `max_width` int(12) DEFAULT NULL COMMENT '宽度-最大',
  `max_path` varchar(200) DEFAULT NULL COMMENT '最大图片路径',
  `min_file_size` int(12) DEFAULT NULL COMMENT '文件字节数-最小',
  `min_height` int(12) DEFAULT NULL COMMENT '高度-最小',
  `min_width` int(12) DEFAULT NULL COMMENT '宽度-最小',
  `min_path` varchar(200) DEFAULT NULL COMMENT '最小图片路径',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `remark` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='存取相关图片信息';

SET FOREIGN_KEY_CHECKS = 1;
