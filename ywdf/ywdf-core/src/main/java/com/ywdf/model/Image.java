package com.ywdf.model;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@TableName("image")
@Data
public class Image extends BaseEntity {
    /**
     * 主题
     */
    private String title;
    /**
     * 摘要
     */
    private String subject;
    /**
     * 扩展名
     */
    private String ext;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 原文件名称
     */
    private String originFileName;
    /**
     * 文件字节数
     */
    private Long normalFileSize;
    /**
     * 高度-正常
     */
    private Integer normalHeight;
    /**
     * 宽度-正常
     */
    private Integer normalWidth;
    /**
     * 正常图片路径
     */
    private String normalPath;
    /**
     * 文件字节数-最小
     */
    private Long minFileSize;
    /**
     * 高度-最小
     */
    private Integer minHeight;
    /**
     * 宽度-最小
     */
    private Integer minWidth;
    /**
     * 最小图片路径
     */
    private String minPath;
    /**
     * 添加时间
     */
    private Timestamp createTime;
    /**
     * 备注
     */
    private String remark;
}
